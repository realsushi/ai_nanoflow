### Links
This is a very classy Nvidia IOT dev board, think similar to a Nintendo Switch

#### Check version
cat /etc/nv_tegra_release
**current R32 3.1**

#### Power limits : needs barrel jack 20W supply source
Run ARM @ full speed (mode 0 = 10W; mode 1 = 5W)
```bash
sudo jetson_clocks
```  
` sudo /usr/sbin/nvpmodel -m 0`
* mode 0 is 4 ARM64 A57 cores @ 1500 MHz | GPU 128 cores @ 925 MHz
* mode 1 is 2 ARM64 A57 cores @ 1500 Mhz | GPU 128 cores @ 924 MHz  
**Rem** Deep learning training will raise cpu temp to 75°C with passive heatsink      
**REM2** Over USB-ssh-powered, the board can only be administred if used at power limit ZERO.  
Indeed 2.5A x 5 V **<** Peak power with 4 ARM cores at full steam power (60% usage) : guess what? at 13 Watts, it does shutdown.  
Mode 1 : with only 2 ARM A57 cores, works perfectly fine on USB power, ie overall power < 13W  
#### Docker
#####Docker options
**Video**
Remarks, typical video capture requires device. But it can only be added if the device is plugged in at docker *run* stage.  
`--device /dev/video0`

**Storage**
This parameters adds persistency to the docker upon exiting.  
`-v /home/user/Document:/root/docs`

**networking**
Full host connectivity  
`--net=host`

**Build**
* `docker build -t firefox .` Hello world  
* `docker run -it --device /dev/video0 -v /home/jetson1/Documents/:/root/docs/ --runtime nvidia --ipc=host -v /tmp/.X11-unix/:/tmp/.X11-unix/ -v /tmp/argus_socket:/tmp/argus_socket --cap-add SYS_PTRACE -e DISPLAY=$DISPLAY jitteam/jetson-nano-tf-gpu`
Docker used for TensorFlow 1.13 tutorial.  

**Templates:**  

* NVIDIA FORUM `sudo docker run -it --rm --runtime nvidia -v /tmp/.X11-unix/:/tmp/.X11-unix [container]`
* NVIDIA CONTAINER `sudo docker run --net=host --runtime nvidia --rm --ipc=host **-v /tmp/.X11-unix/:/tmp/.X11-unix/** -v /tmp/argus_socket:/tmp/argus_socket --cap-add SYS_PTRACE **-e DISPLAY=$DISPLAY** -it nvcr.io/nvidia/l4t-base:r32.2.1`
* FINAL `sudo docker run -it --net=host --runtime nvidia --rm --ipc=host **-v /tmp/.X11-unix/:/tmp/.X11-unix/** -v /tmp/argus_socket:/tmp/argus_socket -v /home/jetson1/Documents:/root/docs --cap-add SYS_PTRACE **-e DISPLAY=$DISPLAY** nvcr.io/nvidia/l4t-base:r32.3.1`


#### Docker SSH/RUN GUI
##### SSH server
```
 ssh-keygen -A  
 service ssh restart  
 passwd $user
```

 Enable X11forwarding in /etc/ssh/sshd_config
```
 X11forwarding yes
 X11displayoffset 10
```

##### Docker X11
```
 ssh -X -v $user@$ip
 ssh -X -C -c blowfish-cbc,arcfour user@ip
```
 **CAMERA using GSTREAMER   
ISN'T CURRRENTLY WORKING WITH X11 FORWARDING**   
 Other gui do works, might be better to try with VNC.  


 [XINIT SCRIPTS](https://unix.stackexchange.com/questions/243195/what-desktop-environment-does-startx-run-and-how-can-i-change-it/243210#243210)
 X11uselocalhost no
 
##### Debug GUI
 **HOST:** echo $DISPLAY  
- `localhost:10.0`  
 **GUEST:** echo $DISPLAY  
- `localhost:11.0`  
- **HOST:** xauth list
```  
 user/unix:12  MIT-MAGIC-COOKIE-1  aaaaaaaaaaaaaaaaaaaaaaaaaaaaa4d2  
  user/unix:10  MIT-MAGIC-COOKIE-1  bbbbbbbbbbbbbbbbbbbbbbbbbbbbb3eb  
  user/unix:11  MIT-MAGIC-COOKIE-1  cccccccccccccccccccccccccccccce3
```
 **GUEST:**  
- `xauth:  file /root/.Xauthority does not exist`  
 
-  `xauth add user/unix:11  MIT-MAGIC-COOKIE-1 ccccccccccccccccccccccccccccce3`
 
##### Check DISPLAY
**GUEST** echo $DISPLAY
- `:11`  
**GUEST** xrandr  
- `can't open display :11`  
**GUEST** export DISPLAY=:10  
**GUEST** xrandr  
```bash
Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 16384 x 16384
eDP-1 connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 294mm x 165mm
   1920x1080    120.02*+  60.01    59.97    59.96    59.93  
   1680x1050     84.94    74.89    69.88    59.95    59.88 
 ```

[logs EGL connection | X DISPLAY](logs/egl_connection_issue.md)
**CSI camera**
* Test with
`
gst-launch-1.0 nvarguscamerasrc ! 'video/x-raw(memory:NVMM),width=300, height=300, framerate=21/1, format=NV12' ! nvvidconv flip-method=0 ! 'video/x-raw,width=960, height=616' ! nvvidconv ! nvegltransform ! nveglglessink -e
`  

`gst-launch-1.0 nvarguscamerasrc ! nvoverlaysink`

#### GSTREAMER PIPELINES

** args to test**
```
gst-launch-0.10 -v rtspsrc location=rtsp://127.0.0.1:8554/test   caps="application/x-rtp,media=(string)video,clock-rate=(int)90000,  encoding-name=(string)H264"  ! fakesink
gst-launch-0.10 -v rtspsrc location=rtsp://127.0.0.1:8554/test   caps="application/x-rtp,media=(string)video,clock-rate=(int)90000,  encoding-name=(string)H264" ! rtpmp2tdepay  ! fakesink
gst-launch-0.10 -v rtspsrc location=rtsp://127.0.0.1:8554/test   caps="application/x-rtp,media=(string)video,clock-rate=(int)90000,  encoding-name=(string)H264" ! rtpmp2tdepay  ! mpegtsdemux  ! fakesink
```
**debug**
`GST_DEBUG="*:2"`

#### TensorFlow-GPU
- developer.download.nvidia.com/compute/redist/jp/v42/tensorflow-gpu/
- developer.download.nvidia.com/compute/redist/jp/v**43**/tensorflow-gpu/

#### Other cmds
- **get major id**
`udevadm info -q property -n /dev/video0 | grep MAJOR`

- **add devices on the go [potentially deprecated]**
echo ‘c 81:* rwm’ > /sys/fs/cgroup/devices/docker/*af1a517573b6/devices.allow

#### TF pipeline
* get config
* Label dataset (train/test) into csv
* **generate TF Record** convert csv to tfrecord
 * python3 packages: python3.6-dev, pandas, cython
* plop
* plop tensorboard

#### Known issues: 
* Get ssh access : Same subnet & ssh
OR plug usb power into thunderbolt3 connector and ssh xxx.xxx.55.x (defautlt, server is nano)
* Get DNS : usually resolv.conf but not here.
* Get X11 : `ssh -X -p 22 user@jetson`
* Failed to write X11 : ll; check permissions on **Xauthority** otherwise rm file and exit;
* Fails on startx: TBD
* Certificate error on apt update : 
  * `timedatectl status`
  * `sudo timedatectl set-time "2020-02-11 14:10:00"`
  * `timedatectl set-ntp true`
  
* Set multiverse and universe
* Protobuf compilation
* model not found:
  ```bash
  cd github/models-master/research
  export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
  protoc object_detection/protos/*.proto --python_out=.
  ```
* model has no attribute compat v1
  ```bash
  cd models/models-1.13/research
  export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
  protoc object_detection/protos/*.proto --python_out=.
  ```
* Camera on Docker doesn't start (check for additional incorrect authentification on HOST just in case)
`nvbuf_utils: Could not get EGL display connection`
#### Monitor
* [JTOP](https://github.com/rbonghi/jetson_stats)
* [PW management](https://docs.nvidia.com/jetson/l4t/index.html#page/Tegra%2520Linux%2520Driver%2520Package%2520Development%2520Guide%2FAppendixTegraStats.html%23wwconnect_header)

#### bluetooth
  `pactl unload-module module-bluetooth-discover
  pactl load-module module-bluetooth-discover`
  
#### Errors
###### set [environment variables](https://github.com/tensorflow/models/blob/df122b1099dae807708e6baaf2c3d6398190ce7a/research/object_detection/g3doc/installation.md)
* protobuf
* python slim
###### set absolute PATHS in model.config
```python
raise ValueError("No variables to save")
ValueError: No variables to save
```
```python
TypeError: Expected int32, got range(0, 3) of type 'range' instead.
```
```python
tensorflow.python.framework.errors_impl.NotFoundError: /models/research/faster_rcnn_resnet101_coco_11_06_2017/pet_label_map.pbtxt; No such file or directory
```

#### APPENDIX : tests on nintendo switch
* docker crash for official nvidia docker image
* docker run for training env



